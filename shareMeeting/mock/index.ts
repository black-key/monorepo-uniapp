import { MockMethod } from 'vite-plugin-mock'

export default [
  {
    url: '/api/mock/test',
    method: 'post',
    response: () => {
      return {
        data: {
          'list|1-10': [
            {
              'id|+1': 1,
              name: '@name'
            }
          ]
        },
        code: 200,
        msg: 'ok'
      }
    }
  }
] as MockMethod[]
