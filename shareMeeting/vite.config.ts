import uni from '@dcloudio/vite-plugin-uni'
import { viteMockServe } from 'vite-plugin-mock'
import { fileURLToPath, URL } from 'node:url'
import { defineConfig } from 'vite'

// // https://vitejs.dev/config/

export default ({ command }) => {
  return defineConfig({
    plugins: [
      uni(),
      viteMockServe({
        mockPath: 'mock',
        localEnabled: command === 'serve'
      })
    ],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    server: {
      proxy: {
        '/api': {
          target: 'http://baidu.com',
          rewrite(path) {
            return path.replace('/api', '')
          }
        }
      }
    }
  })
}
