import { getHttp } from '@weWork/utils/http'

const http = getHttp('/api', '/api')

export default http
