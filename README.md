## 共享办公移动端

本项目使用`monorepo`架构,初次使用请详细阅读本文档

### 包管理器

**必须使用`pnpm`来安装依赖!**

**必须使用`pnpm`来安装依赖!**

**必须使用`pnpm`来安装依赖!**

全局安装`pnpm`

```shell
npm i -g pnpm
```

### 安装依赖

在根目录下执行`pnpm i`即可,会自动安装所有项目的依赖,会自动将共同的依赖提升到根目录下的`node_modules`中

```shell
pnpm i
```

### 运行/构建项目

在根目录下面执行`pnpm run dev:xxx`或者`pnpm run build:xxx`即可,不用切换到到对应的目录中

### 使用公共依赖

直接导入即可,`from`后面跟的是该模块的`package.json`中的`name`值,例如:

```js
import { test } from '@weWork/utils'
```

### 新增依赖

在各自项目中使用`pnpm i xxx`安装即可,多个项目相同的依赖会自动提升,不会重复安装

### 新增公共模块

1. 在根目录下新建文件夹,名字为该模块名,例如`test`

2. 在根目录下的`pnpm-workspace.yaml`中注册,名字为该模块名,例如`test`

3. 在该模块的目录下执行`pnpm init`初始化一个`package.json`,将其中的`name`字段改为`@weWork/xxx`,例如`@weWork/test`

4. 在根目录下使用`pnpm i @weWork/xxx -w`将该模块安装到工作空间,`pnpm i @weWork/xxx -w`,例如`pnpm i @weWork/test -w`

### 新增项目

1. 在仓库根目录下执行`npx degit dcloudio/uni-preset-vue#vite-ts projectName`即可

```shell
npx degit dcloudio/uni-preset-vue#vite-ts projectName
```

2. 在根目录下的`pnpm-workspace.yaml`中注册,名字为该模块名,否则不会自动安装依赖

### 仓库结构说明

```
we-work-mobile
├── bin // 公用的node脚本,一般是开发的时候使用的,不会在线上使用
├── components // 公用的组件模块,新增的组件需要在components/index中导出
├── utils // 公用的工具函数
├── space // 共享空间项目
├── meeting // 共享会议项目
└── xxx // 其他项目
```
