export const setToken = (token: string) => {
  uni.setStorageSync('token', token)
}

export const getToken = () => {
  const token = uni.getStorageSync('token')
  return token
}
