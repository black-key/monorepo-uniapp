export type Callback = () => void

export type CommonParams = {
  [key: string]: string | number | Array<string> | Array<number> | boolean
}
